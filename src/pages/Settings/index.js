import React from 'react';

import { Container, Title, Button, ButtonText } from './styles'

export default function Settings({ navigation }) {
  return (
    <Container>
    <Title>Settings - Modulo Teste</Title>
    <Button onPress={() => navigation.navigate('About')}>
      <ButtonText>Ir para About</ButtonText>
    </Button>
  </Container>
  );
}
