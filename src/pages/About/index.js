import React from 'react';

import { Container, Title, Button, ButtonText } from './styles'

export default function About({ navigation }) {
  return ( 
    <Container>
      <Title>About - Modulo Teste</Title>
      <Button onPress={() => navigation.navigate('Home')}>
        <ButtonText> Ir para a Home</ButtonText>
      </Button>
      <Title>Teste 1</Title>
    </Container>
  );
}
