import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './pages/Home';
import Settings from './pages/Settings';
import About from './pages/About';

const Stack = createStackNavigator();

const Routes = () => {
  return (
    <Stack.Navigator sceneAnimationEnabled={false}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="Settings" component={Settings} />
      <Stack.Screen name="About" component={About} />
    </Stack.Navigator>
  );
};

export default Routes;
