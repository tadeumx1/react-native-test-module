import React from 'react';
import { NavigationContainer } from '@react-navigation/native';

import Routes from './routes';

export default function App({ information }) {
  console.log('information')
  console.log(information)
  return (
    <NavigationContainer independent={true}>
      <Routes />
    </NavigationContainer>
  );
}
